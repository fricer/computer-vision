function ret = show_point_pairs(img1, img2, f1, f2, matches)

        s = size(matches);
        subset_size = 10;
        subset_ind = randi([1 s(2)], 1, subset_size);
        subset_matches = matches(:, subset_ind);

        %%% visualization of the pairs
        figure(2) ; clf ;

        %% padding smaller to the size of the bigger one (for concatenation)
        if(size(img1)>size(img2))
            zeros_im = zeros(size(img1));
            smallSize = size(img2);
            zeros_im(1:smallSize(1), 1:smallSize(2)) = img2;
            img2 = zeros_im;            
        else
            zeros_im = zeros(size(img2));
            smallSize = size(img1);
            zeros_im(1:smallSize(1), 1:smallSize(2)) = img1;
            img1 = zeros_im;          
        end
        
        imshow(cat(2, img1, img2)) ;

        xa = f1(1,subset_matches(1,:)) ;
        xb = f2(1,subset_matches(2,:)) + size(img1,2) ;
        ya = f1(2,subset_matches(1,:)) ;
        yb = f2(2,subset_matches(2,:)) ;

        hold on ;
        h = line([xa ; xb], [ya ; yb]) ;
        set(h,'linewidth', 2, {'color'}, num2cell(jet(subset_size), 2)) ;

        vl_plotframe(f1(:,subset_matches(1,:))) ;
        f2(1,:) = f2(1,:) + size(img1,2) ;
        vl_plotframe(f2(:,subset_matches(2,:))) ;
        axis image off ;
        ret = 0;
end
