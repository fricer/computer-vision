img1 = imread('left.jpg');
img2 = imread('right.jpg');

[f1, f2, matches] = find_matches_VLFEAT(img1, img2);

perm = randperm(size(matches,2)) ;
sel = perm(1:10) ;
matches_sample = matches(:,sel);

f1 = f1(:,matches_sample(1,:));
f2 = f2(:,matches_sample(2,:));

% shift the x coordinate of right image with the width of left image
f2(1,:) = f2(1,:) + size(img1, 1); 

montage({img1, img2});

h1 = vl_plotframe(f1) ;
h2 = vl_plotframe(f2);
set(h1,'color','r','linewidth',2) ;
set(h2,'color','y','linewidth',2) ;

x1 = f1(1,:);
x2 = f2(1,:);
y1 = f1(2,:);
y2 = f2(2,:);

h = line([x1; x2], [y1; y2]);
set(h,'linewidth', 1);