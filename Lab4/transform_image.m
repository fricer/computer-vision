function transformed = transform_image(im, T_params, inverse, size_im)

    if ~exist('inverse', 'var')
        inverse = false;
    end
    
    if ~exist('size_im', 'var')
        size_im = size(im);
    end

    % Get transformation matrix
    
    if size(T_params,2)==1
        tmp = num2cell(T_params); 
        [m1, m2, m3, m4, t1, t2] = tmp{:};

        M = [m1 m2 t1; m3 m4 t2; 0 0 1];
    else
        M = T_params;
    end
    
    if inverse
        M = inv(M);
    end
    
    % Get image size
    
    if isstring(size_im) && size_im == "fit"
        
        size_im = size(im);
        corners = [...
            1, size_im(2),  1,          size_im(2); ...
            1, 1,           size_im(1), size_im(1); ...
            1, 1,           1,          1];
        tr_corners = round(M*corners);
        
        max_height = max(tr_corners(2, :));
        min_height = min(tr_corners(2, :));
        new_height = max_height - min_height;

        max_width = max(tr_corners(1, :));
        min_width = min(tr_corners(1, :));
        new_width = max_width - min_width;
        
        size_im = [new_height, new_width];
        M = M + [0, 0, 1-min_width; 0, 0, 1-min_height; 0 , 0 , 0];
    end
    
    if size(size_im)<3
        size_im = [size_im, size(im,3)];
    end
    
    % Get all points in image
    
    [xx, yy] = meshgrid(1:size_im(2), 1:size_im(1));
    points = [reshape(xx, 1, []); reshape(yy, 1, []); ones(1, numel(xx))];
    
    % Get points they originated from
    
    mapped_points = round(M\points);
    
    % Crop pixels mapped to quadrants 2 to 4
    
    non_neg = all(mapped_points>0,1);
    
    points = points(:, non_neg);
    mapped_points = mapped_points(:, non_neg);
    
    % Crop pixel mapped outside the image size
    
    in_frame = mapped_points(1,:)<size(im,2) & mapped_points(2,:)<size(im,1);
    
    points = points(:, in_frame);
    mapped_points = mapped_points(:, in_frame);
    
    % Get points indexes
    
    idx = sub2ind(size_im, ...
        repmat(points(2,:), [1,size_im(3)]), ...
        repmat(points(1,:), [1,size_im(3)]), ...
        repelem(1:size_im(3), size(points,2)));

    idx_mapped = sub2ind(size(im), ...
        repmat(mapped_points(2,:), [1,size_im(3)]), ...
        repmat(mapped_points(1,:), [1,size_im(3)]), ...
        repelem(1:size_im(3), size(points,2)));
    
    % Get color from original points
    
    transformed = zeros(size_im);
    transformed(idx) = im(idx_mapped);
end