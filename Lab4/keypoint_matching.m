function [f1, f2, matches] = keypoint_matching(img1, img2, plot)

        
    
    if ~exist('plot', 'var')
        plot = false;
    end

    if size(img1,3) == 3
        img1 = rgb2gray(img1);
    end
    
    if size(img2, 3) == 3
        img2 = rgb2gray(img2);
    end

    
    %% in case we are plotting, extract less features so the connections are visible

    [f1, d1] = vl_sift(single(img1));
    [f2, d2] = vl_sift(single(img2));
  
    
    [matches, ~] = vl_ubcmatch(d1, d2);
    
    if plot
        show_point_pairs(img1, img2, f1, f2, matches);
    end
end