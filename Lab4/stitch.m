function [stitched] = stitch(left, right, plot, n, p, alpha)

    if ~exist('plot', 'var')
        plot = false;
    end

    if ~exist('n', 'var')
        n = 100;
    end
    
    if ~exist('p', 'var')
        p = 20;
    end
    
    if ~exist('alpha', 'var')
        alpha = 5;
    end
    
    % 1. Find best transf,orm between images
    T = RANSAC(right, left, n, p, alpha);
    T = [T(1) T(2) T(5); T(3) T(4) T(6); 0 0 1];
    
    
    % 2. Estimate size of stitched image
    
    % get transformed coordinates of right
    [h, w] = size(right, [1,2]);
    
    top_left = round(T*[1; 1; 1]);
    top_right = round(T*[w; 1; 1]);
    bottom_left = round(T*[1; h; 1]);
    bottom_right = round(T*[w; h; 1]);
    
    max_height = max([top_left(2), top_right(2), bottom_left(2), bottom_right(2), size(left,1)]);
    min_height = min([top_left(2), top_right(2), bottom_left(2), bottom_right(2), 1]);
    new_height = max_height - min_height;

    max_width = max([top_left(1), top_right(1), bottom_left(1), bottom_right(1), size(left, 2)]);
    min_width = min([top_left(1), top_right(1), bottom_left(1), bottom_right(1), 1]);
    new_width = max_width - min_width;
    
    % Create transformation with shift in new coordinate system
    
    shift = [0, 0, 1-min_width; 0, 0, 1-min_height; 0 , 0 , 0];
    
    % Put transformed image in new coordinate system
    
    transformed_left = transform_image(right, T+shift, false, [new_height, new_width]);
    
    % Relocate original (right)
    
    transformed_right = transform_image(left, eye(3)+shift, false, [new_height, new_width]);
    
    % Compose images
    
    stitched_right_top = transformed_left;
    stitched_left_top = transformed_right;
    
    non_zero_right = repmat(all(transformed_right>0,3), [1,1, size(right,3)]);
    non_zero_left = repmat(all(transformed_left>0,3), [1,1, size(left,3)]);
    non_zero_both = repmat(all(transformed_left>0 & transformed_right>0,3), [1,1, size(left,3)]);
    
    stitched_right_top(non_zero_right) = transformed_right(non_zero_right);
    stitched_left_top(non_zero_left) = transformed_left(non_zero_left);
    
    stitched = stitched_right_top;
    avg = (transformed_left + transformed_right)/2.0;
    stitched(non_zero_both) = avg(non_zero_both);
    
    % Show image

    if plot
        figure;

        tiledlayout(3,2);

        nexttile;

        imshow(left);
        title("Original image 1");

        nexttile;

        imshow(right);
        title("Original image 2")

        nexttile([1,2]);

        imshow(mat2gray(stitched));
        title("Averaged");

        nexttile;

        imshow(mat2gray(stitched_right_top));
        title("Right image over Left image");

        nexttile;
    
        imshow(mat2gray(stitched_left_top));
        title("Left image over Right image");

    end
    
end