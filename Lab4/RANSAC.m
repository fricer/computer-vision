function [T_params, inliers] = RANSAC(im1, im2, num_iterations, sample_size, threshold, plot)

    if ~exist('plot', 'var')
        plot = false;
    end

    [f1, f2, T] = keypoint_matching(im1, im2, plot);

    num_inliers = 0;
    T_params = zeros(1,6);

    for n = 1:num_iterations
        % pick P random samples out of matches T
        P = randsample(size(T, 2), sample_size);

        x1 = f1(1, T(1, P));
        x2 = f2(1, T(2, P));
        y1 = f1(2, T(1, P));
        y2 = f2(2, T(2, P));

        % construct A
        A = [x1' y1' zeros(size(x1')) zeros(size(x1')) ones(size(x1')) zeros(size(x1'));
            zeros(size(x1')) zeros(size(x1')) x1' y1' zeros(size(x1')) ones(size(x1'))];

        % construct b
        b = [x2'; y2'];

        x = pinv(A) * b;

        % using x, transform all T points in image 1
        tmp = num2cell(x); 
        [m1, m2, m3, m4, t1, t2] = tmp{:};

        T_x_f1 = f1(1, T(1,:));
        T_y_f1 = f1(2, T(1,:));
        transformed = [m1, m2; m3, m4] * [T_x_f1; T_y_f1] + [t1; t2];

        % clf;
        % showMatchedFeatures(im1,im2,[T_x_f1;T_y_f1]',transformed','montage');

        % Count number of inliers (transformed points in img1) in 10 pixel radius
        % from pair in img2
        T_x_f2 = f2(1, T(2,:));
        T_y_f2 = f2(2, T(2,:));

        % calculate Eucledian distance
        distance = sqrt((transformed(1,:) - T_x_f2).^2 + (transformed(2,:) - T_y_f2).^2);

        % keep only inliers
        transformed(:,distance > threshold) = [];

        % if count inliers > best => save the transformation params
        if size(transformed, 2) > num_inliers
           T_params = x;
           num_inliers = size(transformed, 2);
           inliers = transformed;
        end

    end
    
    %% Plotting
    
    if plot
       
        % transform im1 in im2
        transformed_im1 = transform_image(im1, T_params);
        transformed_im1_fit = transform_image(im1, T_params, false, "fit");
        
        affine = affine2d([...
            T_params(1) T_params(2) T_params(5);...
            T_params(3) T_params(4) T_params(6);...
            0 0 1]');
        
        transformed_im1_ml = imwarp(im1, affine);
        
        % show im1 transformed
        
        figure;
        
        tiledlayout(3,2);
        
        nexttile([1,2]);
        imshow(im1);
        title("Original image 1");
        
        nexttile;
        imshow(im2)
        title("Original image 2");
        
        nexttile;
        imshow(mat2gray(transformed_im1));
        title("Transformed image 1");
        
        nexttile;
        imshow(mat2gray(transformed_im1_fit));
        title("Transformed image 1 (fit)");
        
        nexttile;
        imshow(transformed_im1_ml);
        title("Transformed image using imwarp");
        
        % transform im2 in im1
        transformed_im2 = transform_image(im2, T_params, true);
        transformed_im2_fit = transform_image(im2, T_params, true, "fit");
        
        affine = invert(affine);
        transformed_im2_ml = imwarp(im2, affine);
        
        % show im1 transformed
        
        figure;
        
        tiledlayout(3,2);
        
        nexttile([1,2]);
        imshow(im2);
        title("Original image 2");
        
        nexttile;
        imshow(im1)
        title("Original image 1");
        
        nexttile;
        imshow(mat2gray(transformed_im2));
        title("Transformed image 2");
        
        nexttile;
        imshow(mat2gray(transformed_im2_fit));
        title("Transformed image 2 (fit)");
        
        nexttile;
        imshow(transformed_im2_ml);
        title("Transformed image using imwarp");
        
    end
    
end