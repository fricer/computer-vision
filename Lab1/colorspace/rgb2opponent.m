function [output_image] = rgb2opponent(input_image)
% converts an RGB image into opponent color space

output_image = zeros(size(input_image));

for row = 1:size(input_image, 1)
    for col = 1:size(input_image,2)
        r = input_image(row,col,1);
        g = input_image(row,col,2);
        b = input_image(row,col,3);
        output_image(row,col,1) = (r-g)/sqrt(2);
        output_image(row,col,2) = (r+g-2*b)/sqrt(6);
        output_image(row,col,3) = (r+g+b)/sqrt(3);
    end
end

end

