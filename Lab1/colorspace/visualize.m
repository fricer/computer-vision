function visualize(input_image)
    if size(input_image,1) == 4
        subplot(2,2,1), imshow(squeeze(input_image(1,:,:))), title('Lightness');
        subplot(2,2,2), imshow(squeeze(input_image(2,:,:))), title('Average');
        subplot(2,2,3), imshow(squeeze(input_image(3,:,:))), title('Luminosity');
        subplot(2,2,4), imshow(squeeze(input_image(4,:,:))), title('Matlab built-in');
    else
        subplot(1,4,1), imshow(input_image), title('Converted Image');
        subplot(1,4,2), imshow(input_image(:,:,1)), title('Channel 1');
        subplot(1,4,3), imshow(input_image(:,:,2)), title('Channel 2');
        subplot(1,4,4), imshow(input_image(:,:,3)), title('Channel 3');
    end
end

