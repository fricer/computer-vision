function [output_image] = rgb2normedrgb(input_image)
% converts an RGB image into normalized rgb

output_image = zeros(size(input_image));

for row = 1:size(input_image, 1)
    for col = 1:size(input_image,2)
        r = input_image(row,col,1);
        g = input_image(row,col,2);
        b = input_image(row,col,3);
        
        norm = r+g+b;
        
        output_image(row,col,1) = (r)/norm;
        output_image(row,col,2) = (g)/norm;
        output_image(row,col,3) = (b)/norm;
    end
end

end

