function [output_image] = rgb2grays(input_image)
% converts an RGB into grayscale by using 4 different methods

[h, w, ~] = size(input_image);
output_image = zeros(4,h,w);

% ligtness method

for row = 1:h
    for col = 1:w
        r = input_image(row,col,1);
        g = input_image(row,col,2);
        b = input_image(row,col,3);
        output_image(1,row,col) = (max([r,g,b]) + min([r,g,b]))/2;
    end
end

% average method

for row = 1:h
    for col = 1:w
        r = input_image(row,col,1);
        g = input_image(row,col,2);
        b = input_image(row,col,3);
        output_image(2,row,col) = (r+g+b)/3;
    end
end

% luminosity method

for row = 1:h
    for col = 1:w
        r = input_image(row,col,1);
        g = input_image(row,col,2);
        b = input_image(row,col,3);
        output_image(3,row,col) = 0.21*r + 0.72*g + 0.07*b;
    end
end

% built-in MATLAB function 

output_image(4,:,:) = rgb2gray(input_image);
 
end

