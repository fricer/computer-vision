close all
clear all
clc
 
disp('Intrinsic image formation')

% obtain many images in a fixed view under different illumination
disp('Loading images...')

albedo = imread('ball_albedo.png');
%normalizing the rgb values into [0,1] so the multiplication stays in range
albedo = double(albedo) ./255;
shading = imread('ball_shading.png');
shading = double(shading) ./255;

%showing intrinsics
fprintf('Finish loading images.\n\n');
figure()
imshow(albedo)
figure()
imshow(shading)

%performing and showing the reconstruction
reconst = albedo .* shading;
figure()
imshow(reconst) 

%showing original for comparison
full = imread('ball.png');
figure()
imshow(full)

