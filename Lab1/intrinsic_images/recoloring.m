%this code is mostly the same iid_image_formation, except for the
%highlighted part
close all
clear all
clc
 
disp('Intrinsic image formation')

% obtain many images in a fixed view under different illumination
disp('Loading images...')

albedo = imread('ball_albedo.png');
%normalizing the rgb values into [0,1] so the multiplication stays in range

%%swapping the color for new values
newVal = [0, 255, 0];
black = [0,0,0];    %%ignoring background, to only swap the object's albedo
for x = 1: size(albedo, 1)
    for y = 1: size(albedo, 2)
        if albedo(x,y, :) ~= black
            albedo(x,y, :) = newVal;
        end
    end
end
%%

albedo = double(albedo) ./255;
shading = imread('ball_shading.png');
shading = double(shading) ./255;

%showing intrinsics
fprintf('Finish loading images.\n\n');
figure()
imshow(albedo)
figure()
imshow(shading)

%performing and showing the reconstruction
reconst = albedo .* shading;
figure()
imshow(reconst) 

%showing original for comparison
full = imread('ball.png');
figure()
imshow(full)

