function [ p, q, SE ] = check_integrability( normals )
%CHECK_INTEGRABILITY check the surface gradient is acceptable
%   normals: normal image
%   p : df / dx
%   q : df / dy
%   SE : Squared Errors of the 2 second derivatives
% initalization
p = zeros(size(normals, [1 2]));
q = zeros(size(normals, [1 2]));
SE = zeros(size(normals, [1 2]));

% ========================================================================
% YOUR CODE GOES HERE
% Compute p and q, where
% p measures value of df / dx
% q measures value of df / dy

for row = 1:size(normals, 1)
    for col = 1:size(normals,2)
        a = normals(row, col, 1);
        b = normals(row, col, 2);
        c = normals(row, col, 3);

        p(row,col) = a/c;
        q(row,col) = b/c;
    end
end
% ========================================================================


p(isnan(p)) = 0;
q(isnan(q)) = 0;



% ========================================================================
% YOUR CODE GOES HERE
% approximate second derivate by neighbor difference
% and compute the Squared Errors SE of the 2 second derivatives SE

% the function 'diff' computes the difference between neighbouring
% matrix elem (pixels in this case) => losing one row or column
% padd p and q with zeros before applying diff
p_padded = [zeros(1,size(p,2)); p];
q_padded = [zeros(size(q,1),1), q];

% compute dp / dy
second_der_p = diff(p_padded,1,1); % across rows => in the image y direction
% compute dq / dx
second_der_q = diff(q_padded,1,2); % across columns => in the image x direction

% compute the Squared Errors of the second derivatives SE
for row = 1:size(SE, 1)
    for col = 1:size(SE,2)
        SE(row,col) = (second_der_p(row,col) - second_der_q(row,col))^2;
    end
end

% ========================================================================




end

