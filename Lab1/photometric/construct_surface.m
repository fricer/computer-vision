function [ height_map ] = construct_surface( p, q, path_type )
%CONSTRUCT_SURFACE construct the surface function represented as height_map
%   p : measures value of df / dx
%   q : measures value of df / dy
%   path_type: type of path to construct height_map, either 'column',
%   'row', or 'average'
%   height_map: the reconstructed surface


if nargin == 2
    path_type = 'column';
end



switch path_type
    case 'column'

       height_map = calculateHeightColMajor(p,q);
        % =================================================================
               
    case 'row'
        
        % =================================================================
        height_map = calculateHeightRowMajor(p,q);

        % =================================================================
          
    case 'average'
        
        % =================================================================
        height_map = (calculateHeightColMajor(p,q) + calculateHeightRowMajor(p,q))/2;
        
        % =================================================================
end


end

function [height_map_colMajor] = calculateHeightColMajor(p, q)
    [h, w] = size(p);
    height_map_colMajor = zeros(h, w);
            % =================================================================
        % YOUR CODE GOES HERE
        % top left corner of height_map is zero
        % for each pixel in the left column of height_map
        %   height_value = previous_height_value + corresponding_q_value

        % for each row
        %   for each element of the row except for leftmost
        %       height_value = previous_height_value + corresponding_p_value
        
        for row = 1:h
            for col = 1:w
                height = 0;
                for rowInd = 2 : row
                    height = height + q(rowInd, 1);
                    
                end
                for colInd = 2 : col
                    height = height + p(row, colInd);   
                end
                height_map_colMajor(row, col) = height;
            end
        end
end

function [height_map_rowMajor] = calculateHeightRowMajor(p, q)
    [h, w] = size(p);
    height_map_rowMajor = zeros(h, w);

        for row = 1:h
            for col = 1:w
                height = 0;
                for colInd = 2 : col
                    height = height + p(1, colInd);   
                end
                for rowInd = 2 : row
                    height = height + q(rowInd, col);
                    
                end
     
                height_map_rowMajor(row, col) = height;
            end
        end
end

