close all
clear all
clc
 
disp('Part 1: Photometric Stereo')

% obtain many images in a fixed view under different illumination
disp('Loading images...')
image_dir = './MonkeyGray/';   % TODO: get the path of the script
%image_ext = '*.png';

[image_stack, scriptV] = load_syn_images(image_dir);
[h, w, n] = size(image_stack);
fprintf('Finish loading %d images.\n\n', n);

% compute the surface gradient from the stack of imgs and light source mat
disp('Computing surface albedo and normal map...')
[albedo, normals] = estimate_alb_nrm(image_stack, scriptV);


%% integrability check: is (dp / dy  -  dq / dx) ^ 2 small everywhere?
disp('Integrability checking')
[p, q, SE] = check_integrability(normals);

threshold = 0.005;
SE(SE <= threshold) = NaN; % for good visualization
fprintf('Number of outliers: %d\n\n', sum(sum(SE > threshold)));

%% compute the surface height
height_map = construct_surface( p, q, 'average' );

%% Display
show_results(albedo, normals, SE);
show_model(albedo, height_map);


%% RGB

% obtain many images in a fixed view under different illumination
disp('Loading images...')
image_dir = './SphereColor/';   % TODO: get the path of the script
%image_ext = '*.png';

[image_stack_r, scriptV] = load_syn_images(image_dir, 1);
[image_stack_g,  ~] = load_syn_images(image_dir, 2);
[image_stack_b,  ~] = load_syn_images(image_dir, 3);

image_stack_r(isnan(image_stack_r)) = 0;
image_stack_g(isnan(image_stack_g)) = 0;
image_stack_b(isnan(image_stack_b)) = 0;

[h, w, n] = size(image_stack_r);
fprintf('Finish loading %d images.\n\n', n);

% compute the surface gradient from the stack of imgs and light source mat
disp('Computing surface albedo and normal map...')
[albedo_r,  normals_r] = estimate_alb_nrm(image_stack_r, scriptV);
[albedo_g,  normals_g] = estimate_alb_nrm(image_stack_g, scriptV);
[albedo_b,  normals_b] = estimate_alb_nrm(image_stack_b, scriptV);

gs_image_stack = (image_stack_r+image_stack_g+image_stack_b)/3;
[~, normals] = estimate_alb_nrm(gs_image_stack, scriptV);


%% integrability check: is (dp / dy  -  dq / dx) ^ 2 small everywhere?
disp('Integrability checking')
[p, q, SE] = check_integrability(normals);
[p_r, q_r, SE_r] = check_integrability(normals_r);
[p_g, q_g, SE_g] = check_integrability(normals_g);
[p_b, q_b, SE_b] = check_integrability(normals_b);

threshold = 0.005;
SE(SE <= threshold) = NaN; % for good visualization
SE_r(SE_r <= threshold) = NaN; % for good visualization
SE_g(SE_g <= threshold) = NaN; % for good visualization
SE_b(SE_b <= threshold) = NaN; % for good visualization

fprintf('Number of outliers: %d\n\n', sum(sum(SE > threshold)));
fprintf('Number of outliers red: %d\n\n', sum(sum(SE_r > threshold)));
fprintf('Number of outliers green: %d\n\n', sum(sum(SE_g > threshold)));
fprintf('Number of outliers blue: %d\n\n', sum(sum(SE_b > threshold)));

%% compute the surface height
height_map = construct_surface( p, q, 'average' );

%% Display
show_results(albedo_r, normals_r, SE_r);
%show_model(albedo_r, height_map_r);

show_results(albedo_g, normals_g, SE_g);
%show_model(albedo_g, height_map_g);

show_results(albedo_b, normals_b, SE_b);
%show_model(albedo_b, height_map_b);

albedo_tot = zeros(h,w,3);

albedo_tot(:,:,1) = albedo_r;
albedo_tot(:,:,2) = albedo_g;
albedo_tot(:,:,3) = albedo_b;

show_model(albedo_tot, height_map);


%% Face
[image_stack, scriptV] = load_face_images('./yaleB02/');
[h, w, n] = size(image_stack);
fprintf('Finish loading %d images.\n\n', n);
disp('Computing surface albedo and normal map...')
[albedo, normals] = estimate_alb_nrm(image_stack, scriptV, false);

%% integrability check: is (dp / dy  -  dq / dx) ^ 2 small everywhere?
disp('Integrability checking')
[p, q, SE] = check_integrability(normals);

threshold = 0.005;
SE(SE <= threshold) = NaN; % for good visualization
fprintf('Number of outliers: %d\n\n', sum(sum(SE > threshold)));  

%% compute the surface height
height_map_col = construct_surface( p, q, 'column' );
height_map_row = construct_surface( p, q, 'row' );
height_map_avg = construct_surface( p, q, 'average' );


show_results(albedo, normals, SE);
show_model(albedo, height_map_col);
show_model(albedo, height_map_row);
show_model(albedo, height_map_avg);

%show_model(albedo, p-q);

%show_model(albedo, p);
%show_model(albedo, q);

