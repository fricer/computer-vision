% load image
image = imread("awb.jpg");

result = zeros(size(image));
for ch = 1:size(image,3)
    %get channel
    channel = image(:,:,ch);
    
    % calculate mean
    channel_mean = mean(channel, 'all');
    
    % calculate scale
    scale = 128 / channel_mean;
    
    % update according to scale
    result(:,:,ch) = scale * channel;
end

% show result
imshow([image, result]);
