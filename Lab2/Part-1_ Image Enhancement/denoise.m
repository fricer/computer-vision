function [ imOut ] = denoise( image, kernel_type, varargin)

size = 3;
sigma = 0.25;

if nargin > 2
    size = varargin{1};
    
    if nargin > 3
        sigma = varargin{2};
    end
end

% imOut = zeros(size(image, 1), size(image, 1));
switch kernel_type
    case 'box'
        imOut = imboxfilt(image, size);
    case 'median'
        imOut = medfilt2(image, [size size]);
    case 'gaussian'
        kernel = gauss2D(sigma, size);
        imOut = imfilter(image, kernel);
end
end
