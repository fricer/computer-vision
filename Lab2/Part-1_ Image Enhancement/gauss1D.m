 function G = gauss1D( sigma , kernel_size )
 
    G = zeros(1, kernel_size);
    
    if mod(kernel_size, 2) == 0
        error('kernel_size must be odd, otherwise the filter will not have a center to convolve on')
    end
    
    % generate x from - kernel_size/2 to kernel_size/2
    x = ( - floor(kernel_size / 2) : floor(kernel_size / 2));
    %size(G)
%     for i = 1:size(G,2)
%        G(i) = 1 / (sigma * sqrt(2 * pi)) * exp( - (x(i)^2) / (2 * sigma^2) );
%     end
    
    G = 1 / (sigma * sqrt(2 * pi)) * exp( - (x.^2) / (2 * sigma^2) );

    % normalize
    G = G / sum(G);
    %% solution
end
