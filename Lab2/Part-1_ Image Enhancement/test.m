%% Salt and pepper denoising tests
figure()
imshow(image1_saltpepper)
figure()
imshow(denoise(image1_saltpepper, 'box', 3))
figure()
imshow(denoise(image1_saltpepper, 'box', 5))
figure()
imshow(denoise(image1_saltpepper, 'box', 7))
figure()
imshow(denoise(image1_saltpepper, 'median', 3))
figure()
imshow(denoise(image1_saltpepper, 'median', 5))
figure()
imshow(denoise(image1_saltpepper, 'median', 7))
figure()
imshow(denoise(image1_saltpepper, 'gaussian', 5, 5))

%% Gaussian denoising tests
figure()
imshow(image1_gaussian)
figure()
imshow(denoise(image1_gaussian, 'box'))
figure()
imshow(denoise(image1_gaussian, 'median'))
figure()
imshow(denoise(image1_gaussian, 'gaussian'))

%% Gradient calculations
compute_gradient(image1)

%% second order derivative filters
figure()
imshow(compute_LoG(image2, 1))
figure()
imshow(compute_LoG(image2, 2))
figure()
imshow(compute_LoG(image2, 3))