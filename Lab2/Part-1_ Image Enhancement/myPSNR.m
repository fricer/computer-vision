function [ PSNR ] = myPSNR( orig_image, approx_image )

    orig_image = double(orig_image);
    approx_image = double(approx_image);

    [M, N] = size(orig_image, [1,2]);
    MSE = sum((orig_image - approx_image).^2, 'all')/(M*N);
    
    I_MAX = max(max(orig_image));
    
    PSNR = 20*log10(I_MAX/sqrt(MSE));

end

