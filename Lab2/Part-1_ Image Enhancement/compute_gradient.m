function [Gx, Gy, im_magnitude,im_direction] = compute_gradient(image)
    x_dir = [1 0 -1; 2 0 -2; 1 0 -1];
    y_dir = [1 2 1; 0 0 0; -1 -2 -1];
    
    Gx = conv2(image, x_dir, 'same');
    Gy = conv2(image, y_dir, 'same');
    
    im_magnitude = sqrt(Gx.^2 + Gy.^2);
    
%     imshow(Gx, [])
%     imshow(Gy, [])
    imshow(im_magnitude, [])
    
    im_direction = atan(Gx ./ Gy);
%     imshow(im_direction, [])
end

