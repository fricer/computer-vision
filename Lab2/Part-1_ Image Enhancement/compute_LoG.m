function imOut = compute_LoG(image, LOG_type)

switch LOG_type
    case 1
        %method 1
        image_denoised = denoise(image, 'gaussian', 5, 0.5);
        imOut = conv2(image_denoised, fspecial('laplacian'));


    case 2
        imOut = conv2(image, fspecial('log', 5, 0.5), 'same');

    case 3
        %method 3
        imOut = conv2(image,gauss2D(0.5, 5)) - conv2(image, gauss2D(0.5/1.6, 5));

end
end

