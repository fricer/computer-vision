figure('Name','Theta');
theta = (0:0.5:1);

for t = 1:size(theta,2)
    gabor = createGabor(30,theta(t),15,1,1);
    subplot(230 + t), imshow(gabor(:,:,1),[]);
    title(theta(t));
    subplot(233 + t), imshow(gabor(:,:,2),[]);
    title(theta(t));
end

figure('Name','Sigma');
sigma = (10:10:30);

for s = 1:size(sigma,2)
    gabor = createGabor(sigma(s),0.75,15,1,1);
    subplot(230 + s), imshow(gabor(:,:,1),[]);
    title(sigma(s));
    subplot(233 + s), imshow(gabor(:,:,2),[]);
    title(sigma(s));
end

figure('Name','Gamma');
gamma = [0.1 0.5 1];

for g = 1:size(gamma,2)
    gabor = createGabor(30,0.75,15,1,gamma(g));
    subplot(230 + g), imshow(gabor(:,:,1),[]);
    title(gamma(g));
    subplot(233 + g), imshow(gabor(:,:,2),[]);
    title(gamma(g));
end

