img = imread('person_toy/00000001.jpg');

img_rot45 = imrotate(img, 45);
img_rot90 = imrotate(img, 90);

[~, r_orig, c_orig] = harris_corner_detector(img, 5, 1, 3, 0.03, false);
[~, r_45, c_45] = harris_corner_detector(img_rot45, 5, 1, 3, 0.03, false);
[~, r_90, c_90] = harris_corner_detector(img_rot90, 5, 1, 3, 0.03, false);


subplot(131);
imshow(img);
hold on;
plot(c_orig,r_orig, 'r*');

subplot(132);
imshow(img_rot45);
hold on;
plot(c_45,r_45, 'r*');

subplot(133);
imshow(img_rot90);
hold on;
plot(c_90,r_90, 'r*');