function [Vx, Vy] = lucas_kanade(im1, im2, per_pixel, size_n, grid, show_plot)
    
    if ~exist('per_pixel', 'var')
        per_pixel = false;
    end
    
    if ~exist('size_n', 'var')
        size_n = 15;
    end
    
    if ~exist('grid', 'var')
        grid = true;
    end
    
    if ~exist('show_plot', 'var')
        show_plot = true;
    end
    

    im1_rgb = im2double(im1);
    im2_rgb = im2double(im2);
    
    if length(size(im1))>2
        im1 = rgb2gray(im1_rgb);
        im2 = rgb2gray(im2_rgb);
    end
    
    if per_pixel
        method = 'same';
    else
        method = 'valid';
    end
    
    size_im = size(im1) - size_n + 1;
    sum_f = ones(size_n);
    
    % Compute derivatives (w.r.t. x, y, and time)
    
    Gt = fspecial('gaussian',3, 0.5);
    
    [Ix, Iy] = compute_gradient(im2);
    It = conv2(im2, Gt, 'same') - conv2(im1, Gt, 'same');
    
    % Compute value of A^TA for each window = [Ixx, Ixy; Ixy, Iyy]
    
    Ixx = conv2(Ix.^2, sum_f, method);
    Iyy = conv2(Iy.^2, sum_f, method);
    Ixy = conv2(Ix.*Iy, sum_f, method);
    
    if ~per_pixel
        Ixx = Ixx(1:size_n:size_im(1), 1:size_n:size_im(2));
        Iyy = Iyy(1:size_n:size_im(1), 1:size_n:size_im(2));
        Ixy = Ixy(1:size_n:size_im(1), 1:size_n:size_im(2));
    end
        
    % Compute value of (A^TA)^-1 for each window = 1/det((A^TA)^-1)[Iyy, -Ixy; -Ixy, Ixx]
    % we will call (A^TA)^-1 as B
    
    det_ATA = 1./(Ixx.*Iyy - Ixy.^2);
    
    B_11 = det_ATA.*Iyy;
    B_12 = det_ATA.*(-Ixy); % = B_21
    B_22 = det_ATA.*Ixx;
    
    % Compute value of A^Tb for each window = [-Ixt; -Iyt]
    
    Ixt = conv2(Ix.*(-It), sum_f, method);
    Iyt = conv2(Iy.*(-It), sum_f, method);
    
    if ~per_pixel
        Ixt = Ixt(1:size_n:size_im(1), 1:size_n:size_im(2));
        Iyt = Iyt(1:size_n:size_im(1), 1:size_n:size_im(2));
    end
        
    % Compute Vx and Vy per window = [B_11*Ixy+B_12*Iyt; B_12*Ixt+B_22*Iyt]
     
    Vx = B_11.*Ixt + B_12.*Iyt;
    Vy = B_12.*Ixt + B_22.*Iyt;
    
    % Show

    if show_plot
        
        figure;
        
        % Show image (with grid)
        
        if grid
            im1_rgb(size_n:size_n:size(im1,1), :, :) = 0;
            im1_rgb(:, size_n:size_n:size(im1,2), :) = 0;
        end
        imshow(im1_rgb);

        hold on;

        % Show velocities
        
        if ~per_pixel
            step_size = size_n;
            start = ceil(size_n/2);
        else
            step_size = 1;
            start = 1;
        end
        
        [X, Y] = meshgrid(...
            start:step_size:(start + (size(Vx,2)-1)*step_size),...
            start:step_size:(start + (size(Vx,1)-1)*step_size)...
        );
        
        q = quiver(X, Y, Vx,Vy);
        q.LineWidth = 2;
    end
end