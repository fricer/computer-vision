function tracking(frames_directory, per_pixel_v, video_name)

    pause on;
    
    if ~exist('per_pixel_v', 'var')
        per_pixel_v = false;
    end

%   Prepare video write if necessary

    if exist('video_name', 'var')
        fprintf("opening video named " + video_name + ".avi...");
        
        video = VideoWriter(video_name + ".avi");
        open(video);
        
        fprintf('done\n');
    end
    
%   Define frame creator function

    function show_features(im, X, Y, Vx, Vy, size_n, per_pixel_v)
        hold off
        clf;

        % Plot image
        axes()

        image(im);
        hold on;

        % Plot features
        plot(X, Y, 'r*');

        step_size = size_n;
        start = ceil(size_n/2);

        if per_pixel_v
            Vx = Vx(round(size_n/2):size_n:size(Vx,1), round(size_n/2):size_n:size(Vx,2));
            Vy = Vy(round(size_n/2):size_n:size(Vy,1), round(size_n/2):size_n:size(Vy,2));
        end


        % Plot vector field (flow of velocities)
        [xx, yy] = meshgrid(...
            start:step_size:(start + (size(Vx,1)-1)*step_size),...
            start:step_size:(start + (size(Vx,2)-1)*step_size)...
        );

        q = quiver(yy, xx, Vx',Vy');
        q.LineWidth = 2;
        q.Color = 'b';
        
        % If necessay, update video file
        if exist('video', 'var')
            writeVideo(video, getframe);
        end
    end
    
    
%   Extract frames

    fprintf('loading frames ...');
    
    im_sequence = [];

    %   Get all images in folder
    
    files = dir(fullfile(frames_directory, '*.jpg'));
    if(isempty(files))
        files = dir(fullfile(frames_directory, '*.jpeg'));
    end
    
    %   Create frame sequence
    for i = 1:length(files)
        
        %   Get current frame
        frame = imread(fullfile(frames_directory, files(i).name));
        %   Append frame
        im_sequence = cat(4, im_sequence, frame);
        
    end
    
    fprintf(' done\n');
    
%   Extract dimension and set starting parameters

    fprintf('extranging size first frame ...');

    im = im_sequence(:, :, :, 1);

    size_n = 15;
    
    size_im = size(im, [1, 2]);
    size_fil = size_im - size_n +1;

    fprintf(' done\n');

%   Extract first set of features
    
    fprintf('Extracting features from first frame...');
    
    %   Get features per pixel
    
    [~, Yc, Xc] = harris_corner_detector(im, 20, 10, size_n, 0.06, false);
    
    %   Convert features in Cartesian coordinates
    
    Xc = double(Xc);
    Yc = double(Yc);
    
    fprintf(' done\n');
    fprintf('Feature flow tracking:\n');
        
%   Track using Optical flow

    for i = 2:size(im_sequence,4)
        
        fprintf("\tComputing frame %d ...", i);
        
        %   Convert cartesian to pixel coordinates of features
        
        Xp = round(Xc, 0);
        Yp = round(Yc, 0);
        
        %   Clip X and Y values
        
        Xp(Xp<1) = 1;
        Yp(Yp<1) = 1;
        
        Xp(Xp>size_im(2)) = size_im(2);
        Yp(Yp>size_im(1)) = size_im(1);
        
        %   Get current and next images
        
        im1 = im_sequence(:, :, :, i-1);
        im2 = im_sequence(:, :, :, i);
        
        %   Get velocities for all widnows
        [Vx_W, Vy_W] = lucas_kanade(im1, im2, per_pixel_v, size_n, false, false);

        %   Generate current output frame
        show_features(im2, Xp, Yp, Vx_W, Vy_W, size_n, per_pixel_v);
        
        %   Convert pixel to window coordinates
        
        if ~per_pixel_v
            X_W = ceil(Xp/size_n);
            Y_W = ceil(Yp/size_n);

            X_W(X_W>size(Vx_W,2)) = size(Vx_W,2);
            Y_W(Y_W>size(Vx_W,1)) = size(Vx_W,1);
        else
            X_W = Xp;
            Y_W = Yp;
        end
        
        %   Get velocities for current features

        W_idx = (X_W-1)*size(Vx_W,1) + Y_W;
        
        Vx = Vx_W(W_idx);
        Vy = Vy_W(W_idx);
        
        %   Update X and Y according to velocity (converted to pixel gradients)
        
        Xc = Xc + size_n*Vx;
        Yc = Yc + size_n*Vy;
        
        fprintf(' done\n');
        
        pause(0.1);
        
    end
    
    % If necessay, close video file
    if exist('video', 'var')
        close(video);
    end

end