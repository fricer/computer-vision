function [Gx, Gy] = compute_gradient(image)
    x_der = [1 0 -1; 2 0 -2; 1 0 -1];
    y_der = x_der';
    
    Gx = conv2(image, x_der, 'same');
    Gy = conv2(image, y_der, 'same');
    
end

