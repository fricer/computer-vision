function [H, r, c] = harris_corner_detector(image, kernel_size, sigma, window_size, threshold, show_plot)

    if ~exist('show_plot', 'var')
        show_plot = true;
    end

    im_rgb = im2double(image);
    im = rgb2gray(im_rgb);
    
%     Get Gaussian filter
    G = fspecial('gaussian',kernel_size, sigma);
    
%     First derivative in x direction
    [Ix, Iy] = compute_gradient(im);
    
    A = conv2(Ix.^2, G, 'same');
    B = conv2(Ix .* Iy, G, 'same');
    C = conv2(Iy.^2, G, 'same');
    
%     Q = [A B; B C];
    
%     Not sure about this one
    H = (A.*C - B.^2) - 0.04 * (A + C).^2;
    
    [r, c] = find((colfilt(H, [window_size window_size], 'sliding', @max)) > threshold);

   if show_plot
       figure(1);
       imshow(Ix);
       
       figure(2);
       imshow(Iy);
       
       figure(3);
       imshow(im_rgb);
       hold on;
       plot(c,r, 'r*');
   end
   
end