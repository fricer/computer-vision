sphere1 = imread("sphere1.ppm");
sphere2 = imread("sphere2.ppm");

synth1 = imread("synth1.pgm");
synth2 = imread("synth2.pgm");

lucas_kanade(sphere1,sphere2);

pause(0.1);

lucas_kanade(synth1,synth2);
