function [classifiers, vocabulary] = train(X_train, y_train, X_vocab, vocabulary_size, sampling_method, sift_descriptor)
    N_classes = length(unique(y_train));

    fprintf('Obtaining visual features from the data... ');

    descriptors = [];
    for i = 1:size(X_vocab, 2)
        d = get_descriptors(X_vocab{i}, sampling_method, sift_descriptor);
        descriptors = cat(2, descriptors, d);
    end
    
    fprintf('Done!\n');
    
    fprintf('Getting vocabulary by k-means... ');
    
    [ vocabulary, ~] = vl_kmeans(double(descriptors), vocabulary_size);
    vocabulary = vocabulary';
    fprintf('Done!\n');
    
    fprintf('Generating histogram representation of the train dataset... ');
    
    histograms = generate_histograms(X_train, vocabulary, sampling_method, sift_descriptor);
    
    fprintf('Done!\n');
    
    fprintf('Training SVM classifiers... ');

    K = N_classes;
    classifiers = cell(1, K);
    for k = 1:K
        % binarize the labels by setting all classes not equal to k to 0
        % and all k to 1
        binary_labels = y_train == k;
        classifiers{k} = fitcsvm(histograms, binary_labels, 'KernelFunction', 'Gaussian');
    end

    fprintf('Done!\n');
end
