function [descriptors] = get_descriptors(image, sampling_method, sift_descriptor)    
    if sift_descriptor == "opponent"
        image = rgb2opponent(image);
    elseif sift_descriptor == "grayscale"
        image = rgb2gray(image);
    end

    descriptors = [];
    n_channels = size(image, 3);
    
    % get descriptors for each channel
    for channel = 1:n_channels
        im = single(image(:, :, channel));
        if sampling_method == "dense"
            [f, ~] = vl_dsift(im, 'size', 25, 'step', 5);
        else
            f = vl_sift(im);
        end
        [~, d] = vl_covdet(im, 'Frames', f, 'descriptor', 'sift');
        
        descriptors = cat(2, descriptors, d);
    end
end