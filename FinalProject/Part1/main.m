% load vlfeat
run('vlfeat-0.9.21/toolbox/vl_setup.m')

N = 2500;
vocabulary_sizes = [400, 1000, 4000];
sampling_methods = ["keypoints", "dense"];
sift_descriptors = ["rgb", "grayscale", "opponent"];
classes = ["airplane", "bird", "ship", "horse", "car"];

fprintf('Load data...');

load('data/train.mat');
X_train = X; 
t_train = y;
load('data/test.mat');
X_test = X; 
y_test = y;

fprintf('Done!\n');

fprintf('Preprosess data...');

% filter dataset to use only the following classes ["airplane", "bird", "ship", "horse", "car"]
[X_train, y_train] = filter_dataset(X_train, t_train, class_names, classes);
[X_test, y_test] = filter_dataset(X_test, y_test, class_names, classes);

X_train = reshape_dataset(X_train);
X_test = reshape_dataset(X_test);

fprintf('Done!\n');

% run the classifier for each combination of settings
for i = 1:length(vocabulary_sizes)
    for j = 1:length(sampling_methods)
        for k = 1:length(sift_descriptors)
            fprintf('Run classifier with vocab_size: %d, sampling method: %s, sift descriptor: %s \n', vocabulary_sizes(i), sampling_methods(j), sift_descriptors(k));
            run_classifier(classes, X_train, X_test, y_train, y_test, vocabulary_sizes(i), sampling_methods(j), sift_descriptors(k), N);
        end
    end
end
