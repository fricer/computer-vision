function [X_train, y_train, X_vocab, y_vocab] = split_train_vocab(X_train, y_train, ratio)
    % split data into vocabulary and bag-of-words
    % make sure that each class is represented equally in the vocabulary
    classes = unique(y_train);
    idxs_vocab = [];
    idxs_train = [];
    for c = 1:size(classes, 1)
       idxs = find(y_train == classes(c));
       N = size(idxs, 1);
       split = floor(N*ratio);
       idxs_v = idxs(1:split);
       idxs_t = idxs(split+1:end);
       idxs_vocab = cat(1, idxs_vocab, idxs_v);
       idxs_train = cat(1, idxs_train, idxs_t);
    end
    
    X_vocab = X_train(idxs_vocab);
    y_vocab = y_train(idxs_vocab);
    
    X_train = X_train(idxs_train);
    y_train = y_train(idxs_train, :);
end