function [labels, scores] = test(X_test, classifiers, vocabulary, sampling_method, sift_descriptor)
    N_classes = length(classifiers);

    fprintf('Generating histogram representation of the test dataset... ');
    
    % generate histograms for images in test dataset
    histograms = generate_histograms(X_test, vocabulary, sampling_method, sift_descriptor);
    
    fprintf('Done!\n');
    
    fprintf('Testing classifiers on the data... ');

    labels = cell(size(N_classes));
    scores = cell(size(N_classes));
    for k = 1:N_classes
        [labels{k}, scores{k}] = predict(classifiers{k}, histograms);
    end
    
    fprintf('Done!\n');
end
