function [] = run_classifier(classes, X_train, X_test, y_train, y_test, vocabulary_size, sampling_method, sift_descriptor, N)
    % shuffle subset
    perm = randperm(size(y_train, 1));
    idxs = perm(1:N);
    X_train = X_train(idxs);
    y_train = y_train(idxs, :);
    
    split_rate = 0.1;
    
    % train the SVM classifiers
    [X_train, y_train, X_vocab, ~] = split_train_vocab(X_train, y_train, split_rate);

    [classifiers, vocabulary] = train(X_train, y_train, X_vocab, vocabulary_size, sampling_method, sift_descriptor);

    % test the SVM classifiers
    [labels, scores] = test(X_test, classifiers, vocabulary, sampling_method, sift_descriptor);

    n_classes = size(classes, 2);
    [APs, mAP, accuracy] = compute_metrics(labels, scores, y_test, n_classes);
    fprintf('mAP: %f, accuracy: %f \n', mAP, accuracy);
    
    % save metrics
    fid = fopen('results/metrics.csv', 'a+');
    fprintf(fid, '%d, %s, %s, %f, %f, %f, %f, %f, %f, %f\n', vocabulary_size, sampling_method, sift_descriptor, APs(1), APs(2), APs(3), APs(4), APs(5), mAP, accuracy);
    fclose(fid);
%     writecell({vocabulary_size sampling_method sift_descriptor APs mAP accuracy}, 'results/metrics.csv', 'append');
    
    % display 5 best and worst
    top_bottom_5(X_test, scores, vocabulary_size, sampling_method, sift_descriptor);
end