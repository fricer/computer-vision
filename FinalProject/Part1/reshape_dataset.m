% Reshape images to 96x96x3 cell array
function [images] = reshape_dataset(X)
    channels = 3;
    [samples, pixels] = size(X);
    side = sqrt(pixels / channels);

    images = cell(1, samples);
    for i = 1:samples
        images{i} = reshape(X(i, :), [side, side, channels]);
    end
end
