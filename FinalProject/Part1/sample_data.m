% sample n rows from our dataset
function [X, y] = sample_data(n, X, y)
    perm = randperm(size(y, 1));
    idxs = perm(1:n);
    X = X(idxs);
    y = y(idxs, :);
end
