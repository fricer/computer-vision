function [] = top_bottom_5(images, scores, vocabulary_size, sampling_method, sift_descriptor)
    num_classes = size(scores, 2);
    for i = 1:num_classes
        score = scores{i}(:, 2);
        [~, idxs] = sort(score, 'descend');
        sorted_images = images(idxs);
        top5 = sorted_images(1:5);
        bottom5 = sorted_images(end-5+1:end);
        figure('visible', 'off');
        montage([top5 bottom5], 'Size', [2 5]);
        filename = sprintf('results/%d_%s_%s_clf%d.png', vocabulary_size, sampling_method, sift_descriptor, i);
        saveas(gcf, filename);
    end
end