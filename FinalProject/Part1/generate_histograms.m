function [histograms] = generate_histograms(X, vocabulary, sampling_method, sift_descriptor)
    histograms = [];
    for i = 1:size(X, 2)
        histogram = get_histogram(X{i}, vocabulary, sampling_method, sift_descriptor);
        histograms = cat(1, histograms, histogram);
    end
end

function [histogram] = get_histogram(image, vocabulary, sampling_method, sift_descriptor)
    vocabulary_size = size(vocabulary, 1);

    descriptors = get_descriptors(image, sampling_method, sift_descriptor); 
    
    distances = pdist2(double(descriptors)', vocabulary);
    [~, dictionary] = min(distances, [], 2);
    histogram = histcounts(dictionary, 1:vocabulary_size+1);
    
    % normalize histogram
    histogram = histogram / sum(sum(histogram));
end
