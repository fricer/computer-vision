function [APs, mAP, accuracy] = compute_metrics(labels, scores, targets, n_classes)
    N = size(labels{1}, 1);
    scores_all = zeros(N, n_classes);
    for i = 1:n_classes
        scores_all(:, i) = scores{i}(:, 2);
    end

    [~, predictions] = max(scores_all, [], 2);
    accuracy = mean(predictions == targets);
    
    % compute mAP
    APs = zeros(1, n_classes);
    for k = 1:n_classes
        [~, idxs] = sort(scores_all(:, k), 'descend');
        preds_sorted = predictions(idxs);
        targets_sorted = targets(idxs);
        m_c = sum(targets == k);
        total = 0;
        correct = 0;
        for i = 1:size(preds_sorted)
            if preds_sorted(i) == k && targets_sorted(i) == k
               correct = correct + 1;
               total = total + correct / i;
            end
        end
        APs(k) = 1 / m_c * total;
    end
    mAP = mean(APs);
end