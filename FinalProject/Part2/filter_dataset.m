function [X, y] = filter_dataset(X, y, all_classes, classes)
    idxs = ismember(y, find(contains(all_classes, classes)));
    X = X(idxs, :);
    y = y(idxs, :);
    
    % map label range to 1 to 5 
    used_labels = contains(all_classes, classes);
    map_labels = @(i) sum(used_labels(1:i));
    y = arrayfun(map_labels, y);
end
