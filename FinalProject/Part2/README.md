
# Installation

After the correct installation of all dependent libraries the following
file strucutre should be present:

1) data           -- dir containing train and test data
2) liblinear-2.1  -- dir containing liblinear source code
3) matconvnet     -- dir containg matconvnet library
4) tSNE_matlab    -- dir containing tsne scirpt


# Run instructions

```
main.m
```
