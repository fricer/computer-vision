function [net, info, expdir] = finetune_cnn(varargin)

%% Define options
run(fullfile(fileparts(mfilename('fullpath')), 'matconvnet', 'matlab', 'vl_setupnn.m')) ;

opts.modelType = 'lenet' ;
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.expDir = fullfile('data', ...
  sprintf('cnn_assignment-%s', opts.modelType)) ;
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.dataDir = './data/' ;
opts.imdbPath = fullfile(opts.expDir, 'imdb-stl.mat');
opts.whitenData = true ;
opts.contrastNormalization = true ;
opts.networkType = 'simplenn' ;
opts.train = struct() ;
opts = vl_argparse(opts, varargin) ;
if ~isfield(opts.train, 'gpus'), opts.train.gpus = []; end;

opts.train.gpus = [];



%% update model

net = load('./data/pre_trained_model.mat');
net = net.net; 
net = update_model();

%% TODO: Implement getIMDB function below

if exist(opts.imdbPath, 'file')
  imdb = load(opts.imdbPath) ;
else
  imdb = getIMDB() ;
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb', '-v7.3') ;
end

%%
net.meta.classes.name = imdb.meta.classes(:)' ;

% -------------------------------------------------------------------------
%                                                                     Train
% -------------------------------------------------------------------------

trainfn = @cnn_train ;
[net, info] = trainfn(net, imdb, getBatch(opts), ...
  'expDir', opts.expDir, ...
  net.meta.trainOpts, ...
  opts.train, ...
  'val', find(imdb.images.set == 2)) ;

%%storing trained net
save('./data/updated_model.mat', '-struct', 'net')

expdir = opts.expDir;
end
% -------------------------------------------------------------------------
function fn = getBatch(opts)
% -------------------------------------------------------------------------
switch lower(opts.networkType)
  case 'simplenn'
    fn = @(x,y) getSimpleNNBatch(x,y) ;
  case 'dagnn'
    bopts = struct('numGpus', numel(opts.train.gpus)) ;
    fn = @(x,y) getDagNNBatch(bopts,x,y) ;
end

end

function [images, labels] = getSimpleNNBatch(imdb, batch)
% -------------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;
if rand > 0.5, images=fliplr(images) ; end

end

% -------------------------------------------------------------------------
function imdb = getIMDB()
% -------------------------------------------------------------------------
% Preapre the imdb structure, returns image data with mean image subtracted
classes = {'airplane', 'bird', 'ship', 'horse', 'car'};
splits = {'train', 'test'};

%% TODO: Implement your loop here, to create the data structure described in the assignment
%% Use train.mat and test.mat we provided from STL-10 to fill in necessary data members for training below
%% You will need to, in a loop function,  1) read the image, 2) resize the image to (32,32,3), 3) read the label of that image

train = load("./data/train.mat");
test = load("./data/test.mat");

num_channels = 3;
[train_size, d] = size(train.X);
[test_size, d] = size(test.X);
size_im = 32;

data = zeros(size_im, size_im, num_channels, (train_size + test_size)/2);
labels = zeros(1,(train_size + test_size)/2);
sets = zeros(1,(train_size + test_size)/2);

startingCounter = 0;
for  split = splits
    disp("split is")
    disp(split)
    if split == "train"
        raw_data = train;
        split_idx = 1;
    else
        raw_data = test;
        split_idx = 2;
    end
    
    [X, y] = filter_dataset(raw_data.X, raw_data.y, raw_data.class_names, classes);
    
    
  
    for im_counter = (startingCounter + 1):(startingCounter + size(X, 1))
        im = X(im_counter - startingCounter, :);
   
        D = reshape(im, [size_im*3, size_im*3, num_channels]);
        %% downsampliing to match the pretrained architecture
        D = imresize(D, (1/3), 'nearest');
        D = im2double(D);
        D = D .* 2 -1;
        data(:,:,:,im_counter) = D;
        labels(im_counter) = y(im_counter- startingCounter);
        sets(im_counter) = split_idx;

    end
    startingCounter = im_counter;
end

%%

disp("sets is")
disp(sets(1:20))
disp("labels")
disp(labels(1:20))
disp("data start")
disp(data(1:5, 1:5, 1, 1))


imdb.images.data = single(data) ;
imdb.images.labels = single(labels) ;
imdb.images.set = sets;
imdb.meta.sets = {'train', 'val'} ;
imdb.meta.classes = classes;
dataSize = size(imdb.images.data);
perm = randperm(dataSize(4));

disp("after storing")
disp("sets is")
disp(imdb.images.set(1:20))
disp("labels")
disp(imdb.images.labels(1:20))
disp("data start")
disp(imdb.images.data(1:5, 1:5, 1, 1))

imdb.images.data = imdb.images.data(:,:,:, perm);
imdb.images.labels = imdb.images.labels(perm);
imdb.images.set = imdb.images.set(perm);

disp("after permutation")
disp("sets is")
disp(imdb.images.set(1:20))
disp("labels")
disp(imdb.images.labels(1:20))
disp("data start")
disp(imdb.images.data(1:5, 1:5, 1, 1))
disp("final size of image data")
disp(size(imdb.images.data))
disp("end")
end
